--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Debian 12.9-1.pgdg110+1)
-- Dumped by pg_dump version 12.9 (Debian 12.9-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: conversations_conversation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conversations_conversation (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL
);


ALTER TABLE public.conversations_conversation OWNER TO postgres;

--
-- Name: conversations_conversation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conversations_conversation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conversations_conversation_id_seq OWNER TO postgres;

--
-- Name: conversations_conversation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conversations_conversation_id_seq OWNED BY public.conversations_conversation.id;


--
-- Name: conversations_conversation_participants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conversations_conversation_participants (
    id integer NOT NULL,
    conversation_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.conversations_conversation_participants OWNER TO postgres;

--
-- Name: conversations_conversation_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conversations_conversation_participants_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conversations_conversation_participants_id_seq OWNER TO postgres;

--
-- Name: conversations_conversation_participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conversations_conversation_participants_id_seq OWNED BY public.conversations_conversation_participants.id;


--
-- Name: conversations_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conversations_message (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    message text NOT NULL,
    conversation_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.conversations_message OWNER TO postgres;

--
-- Name: conversations_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conversations_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conversations_message_id_seq OWNER TO postgres;

--
-- Name: conversations_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conversations_message_id_seq OWNED BY public.conversations_message.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: games_game; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games_game (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(140) NOT NULL,
    caption_title character varying(140),
    description character varying(300) NOT NULL,
    game_type_id integer,
    host_id integer NOT NULL,
    link_tutorial character varying(120)
);


ALTER TABLE public.games_game OWNER TO postgres;

--
-- Name: games_game_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_game_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_game_id_seq OWNER TO postgres;

--
-- Name: games_game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_game_id_seq OWNED BY public.games_game.id;


--
-- Name: games_gametype; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games_gametype (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.games_gametype OWNER TO postgres;

--
-- Name: games_gametype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_gametype_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_gametype_id_seq OWNER TO postgres;

--
-- Name: games_gametype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_gametype_id_seq OWNED BY public.games_gametype.id;


--
-- Name: games_photo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games_photo (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    caption character varying(80) NOT NULL,
    file character varying(100) NOT NULL,
    game_id integer NOT NULL
);


ALTER TABLE public.games_photo OWNER TO postgres;

--
-- Name: games_photo2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games_photo2 (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    caption character varying(80) NOT NULL,
    file character varying(100) NOT NULL,
    game_id integer NOT NULL
);


ALTER TABLE public.games_photo2 OWNER TO postgres;

--
-- Name: games_photo2_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_photo2_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_photo2_id_seq OWNER TO postgres;

--
-- Name: games_photo2_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_photo2_id_seq OWNED BY public.games_photo2.id;


--
-- Name: games_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_photo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_photo_id_seq OWNER TO postgres;

--
-- Name: games_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_photo_id_seq OWNED BY public.games_photo.id;


--
-- Name: lists_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lists_list (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.lists_list OWNER TO postgres;

--
-- Name: lists_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lists_list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lists_list_id_seq OWNER TO postgres;

--
-- Name: lists_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lists_list_id_seq OWNED BY public.lists_list.id;


--
-- Name: lists_list_rooms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lists_list_rooms (
    id integer NOT NULL,
    list_id integer NOT NULL,
    room_id integer NOT NULL
);


ALTER TABLE public.lists_list_rooms OWNER TO postgres;

--
-- Name: lists_list_rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lists_list_rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lists_list_rooms_id_seq OWNER TO postgres;

--
-- Name: lists_list_rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lists_list_rooms_id_seq OWNED BY public.lists_list_rooms.id;


--
-- Name: reservations_bookedday; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservations_bookedday (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    day date NOT NULL,
    reservation_id integer NOT NULL
);


ALTER TABLE public.reservations_bookedday OWNER TO postgres;

--
-- Name: reservations_bookedday_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservations_bookedday_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservations_bookedday_id_seq OWNER TO postgres;

--
-- Name: reservations_bookedday_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservations_bookedday_id_seq OWNED BY public.reservations_bookedday.id;


--
-- Name: reservations_reservation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservations_reservation (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    status character varying(12) NOT NULL,
    check_in date NOT NULL,
    check_out date NOT NULL,
    guest_id integer NOT NULL,
    room_id integer NOT NULL
);


ALTER TABLE public.reservations_reservation OWNER TO postgres;

--
-- Name: reservations_reservation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservations_reservation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservations_reservation_id_seq OWNER TO postgres;

--
-- Name: reservations_reservation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservations_reservation_id_seq OWNED BY public.reservations_reservation.id;


--
-- Name: rest_framework_api_key_apikey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rest_framework_api_key_apikey (
    id character varying(100) NOT NULL,
    created timestamp with time zone NOT NULL,
    name character varying(50) NOT NULL,
    revoked boolean NOT NULL,
    expiry_date timestamp with time zone,
    hashed_key character varying(100) NOT NULL,
    prefix character varying(8) NOT NULL
);


ALTER TABLE public.rest_framework_api_key_apikey OWNER TO postgres;

--
-- Name: reviews_review; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews_review (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    review text NOT NULL,
    accuracy integer NOT NULL,
    communication integer NOT NULL,
    cleanliness integer NOT NULL,
    location integer NOT NULL,
    check_in integer NOT NULL,
    value integer NOT NULL,
    room_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.reviews_review OWNER TO postgres;

--
-- Name: reviews_review_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_review_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_review_id_seq OWNER TO postgres;

--
-- Name: reviews_review_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_review_id_seq OWNED BY public.reviews_review.id;


--
-- Name: rooms_amenity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_amenity (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.rooms_amenity OWNER TO postgres;

--
-- Name: rooms_amenity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_amenity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_amenity_id_seq OWNER TO postgres;

--
-- Name: rooms_amenity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_amenity_id_seq OWNED BY public.rooms_amenity.id;


--
-- Name: rooms_facility; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_facility (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.rooms_facility OWNER TO postgres;

--
-- Name: rooms_facility_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_facility_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_facility_id_seq OWNER TO postgres;

--
-- Name: rooms_facility_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_facility_id_seq OWNED BY public.rooms_facility.id;


--
-- Name: rooms_houserule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_houserule (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.rooms_houserule OWNER TO postgres;

--
-- Name: rooms_houserule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_houserule_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_houserule_id_seq OWNER TO postgres;

--
-- Name: rooms_houserule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_houserule_id_seq OWNED BY public.rooms_houserule.id;


--
-- Name: rooms_photo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_photo (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    caption character varying(80) NOT NULL,
    file character varying(100) NOT NULL,
    room_id integer NOT NULL
);


ALTER TABLE public.rooms_photo OWNER TO postgres;

--
-- Name: rooms_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_photo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_photo_id_seq OWNER TO postgres;

--
-- Name: rooms_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_photo_id_seq OWNED BY public.rooms_photo.id;


--
-- Name: rooms_room; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_room (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(140) NOT NULL,
    description character varying(300) NOT NULL,
    country character varying(2) NOT NULL,
    city character varying(80) NOT NULL,
    price integer NOT NULL,
    address character varying(140) NOT NULL,
    guests integer NOT NULL,
    beds integer NOT NULL,
    bedrooms integer NOT NULL,
    baths integer NOT NULL,
    check_in time without time zone NOT NULL,
    check_out time without time zone NOT NULL,
    instant_book boolean NOT NULL,
    host_id integer NOT NULL,
    room_type_id integer
);


ALTER TABLE public.rooms_room OWNER TO postgres;

--
-- Name: rooms_room_amenities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_room_amenities (
    id integer NOT NULL,
    room_id integer NOT NULL,
    amenity_id integer NOT NULL
);


ALTER TABLE public.rooms_room_amenities OWNER TO postgres;

--
-- Name: rooms_room_amenities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_room_amenities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_room_amenities_id_seq OWNER TO postgres;

--
-- Name: rooms_room_amenities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_room_amenities_id_seq OWNED BY public.rooms_room_amenities.id;


--
-- Name: rooms_room_facilities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_room_facilities (
    id integer NOT NULL,
    room_id integer NOT NULL,
    facility_id integer NOT NULL
);


ALTER TABLE public.rooms_room_facilities OWNER TO postgres;

--
-- Name: rooms_room_facilities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_room_facilities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_room_facilities_id_seq OWNER TO postgres;

--
-- Name: rooms_room_facilities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_room_facilities_id_seq OWNED BY public.rooms_room_facilities.id;


--
-- Name: rooms_room_house_rules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_room_house_rules (
    id integer NOT NULL,
    room_id integer NOT NULL,
    houserule_id integer NOT NULL
);


ALTER TABLE public.rooms_room_house_rules OWNER TO postgres;

--
-- Name: rooms_room_house_rules_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_room_house_rules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_room_house_rules_id_seq OWNER TO postgres;

--
-- Name: rooms_room_house_rules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_room_house_rules_id_seq OWNED BY public.rooms_room_house_rules.id;


--
-- Name: rooms_room_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_room_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_room_id_seq OWNER TO postgres;

--
-- Name: rooms_room_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_room_id_seq OWNED BY public.rooms_room.id;


--
-- Name: rooms_roomtype; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms_roomtype (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.rooms_roomtype OWNER TO postgres;

--
-- Name: rooms_roomtype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_roomtype_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_roomtype_id_seq OWNER TO postgres;

--
-- Name: rooms_roomtype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_roomtype_id_seq OWNED BY public.rooms_roomtype.id;


--
-- Name: tiplists_tiplist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tiplists_tiplist (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.tiplists_tiplist OWNER TO postgres;

--
-- Name: tiplists_tiplist_games; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tiplists_tiplist_games (
    id integer NOT NULL,
    tiplist_id integer NOT NULL,
    game_id integer NOT NULL
);


ALTER TABLE public.tiplists_tiplist_games OWNER TO postgres;

--
-- Name: tiplists_tiplist_games_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tiplists_tiplist_games_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiplists_tiplist_games_id_seq OWNER TO postgres;

--
-- Name: tiplists_tiplist_games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tiplists_tiplist_games_id_seq OWNED BY public.tiplists_tiplist_games.id;


--
-- Name: tiplists_tiplist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tiplists_tiplist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiplists_tiplist_id_seq OWNER TO postgres;

--
-- Name: tiplists_tiplist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tiplists_tiplist_id_seq OWNED BY public.tiplists_tiplist.id;


--
-- Name: tiplists_tiplisttutorial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tiplists_tiplisttutorial (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.tiplists_tiplisttutorial OWNER TO postgres;

--
-- Name: tiplists_tiplisttutorial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tiplists_tiplisttutorial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiplists_tiplisttutorial_id_seq OWNER TO postgres;

--
-- Name: tiplists_tiplisttutorial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tiplists_tiplisttutorial_id_seq OWNED BY public.tiplists_tiplisttutorial.id;


--
-- Name: tiplists_tiplisttutorial_tips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tiplists_tiplisttutorial_tips (
    id integer NOT NULL,
    tiplisttutorial_id integer NOT NULL,
    tip_id integer NOT NULL
);


ALTER TABLE public.tiplists_tiplisttutorial_tips OWNER TO postgres;

--
-- Name: tiplists_tiplisttutorial_tips_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tiplists_tiplisttutorial_tips_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiplists_tiplisttutorial_tips_id_seq OWNER TO postgres;

--
-- Name: tiplists_tiplisttutorial_tips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tiplists_tiplisttutorial_tips_id_seq OWNED BY public.tiplists_tiplisttutorial_tips.id;


--
-- Name: tips_photo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tips_photo (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    caption character varying(80) NOT NULL,
    file character varying(100) NOT NULL,
    "Tip_id" integer NOT NULL
);


ALTER TABLE public.tips_photo OWNER TO postgres;

--
-- Name: tips_photo2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tips_photo2 (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    caption character varying(80) NOT NULL,
    file character varying(100) NOT NULL,
    tip_id integer NOT NULL
);


ALTER TABLE public.tips_photo2 OWNER TO postgres;

--
-- Name: tips_photo2_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tips_photo2_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tips_photo2_id_seq OWNER TO postgres;

--
-- Name: tips_photo2_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tips_photo2_id_seq OWNED BY public.tips_photo2.id;


--
-- Name: tips_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tips_photo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tips_photo_id_seq OWNER TO postgres;

--
-- Name: tips_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tips_photo_id_seq OWNED BY public.tips_photo.id;


--
-- Name: tips_tip; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tips_tip (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(140) NOT NULL,
    caption_title character varying(140),
    description character varying(300) NOT NULL,
    link_tutorial character varying(120),
    host_id integer NOT NULL,
    tip_type_id integer
);


ALTER TABLE public.tips_tip OWNER TO postgres;

--
-- Name: tips_tip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tips_tip_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tips_tip_id_seq OWNER TO postgres;

--
-- Name: tips_tip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tips_tip_id_seq OWNED BY public.tips_tip.id;


--
-- Name: tips_tiptype; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tips_tiptype (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.tips_tiptype OWNER TO postgres;

--
-- Name: tips_tiptype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tips_tiptype_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tips_tiptype_id_seq OWNER TO postgres;

--
-- Name: tips_tiptype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tips_tiptype_id_seq OWNED BY public.tips_tiptype.id;


--
-- Name: users_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    avatar character varying(100) NOT NULL,
    gender character varying(10) NOT NULL,
    bio text NOT NULL,
    birthdate date,
    language character varying(2) NOT NULL,
    currency character varying(3) NOT NULL,
    superhost boolean NOT NULL,
    email_verified boolean NOT NULL,
    email_secret character varying(20) NOT NULL,
    login_method character varying(50) NOT NULL
);


ALTER TABLE public.users_user OWNER TO postgres;

--
-- Name: users_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.users_user_groups OWNER TO postgres;

--
-- Name: users_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_groups_id_seq OWNER TO postgres;

--
-- Name: users_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_groups_id_seq OWNED BY public.users_user_groups.id;


--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users_user.id;


--
-- Name: users_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.users_user_user_permissions OWNER TO postgres;

--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_user_permissions_id_seq OWNED BY public.users_user_user_permissions.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: conversations_conversation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation ALTER COLUMN id SET DEFAULT nextval('public.conversations_conversation_id_seq'::regclass);


--
-- Name: conversations_conversation_participants id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation_participants ALTER COLUMN id SET DEFAULT nextval('public.conversations_conversation_participants_id_seq'::regclass);


--
-- Name: conversations_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_message ALTER COLUMN id SET DEFAULT nextval('public.conversations_message_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: games_game id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_game ALTER COLUMN id SET DEFAULT nextval('public.games_game_id_seq'::regclass);


--
-- Name: games_gametype id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_gametype ALTER COLUMN id SET DEFAULT nextval('public.games_gametype_id_seq'::regclass);


--
-- Name: games_photo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_photo ALTER COLUMN id SET DEFAULT nextval('public.games_photo_id_seq'::regclass);


--
-- Name: games_photo2 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_photo2 ALTER COLUMN id SET DEFAULT nextval('public.games_photo2_id_seq'::regclass);


--
-- Name: lists_list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list ALTER COLUMN id SET DEFAULT nextval('public.lists_list_id_seq'::regclass);


--
-- Name: lists_list_rooms id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list_rooms ALTER COLUMN id SET DEFAULT nextval('public.lists_list_rooms_id_seq'::regclass);


--
-- Name: reservations_bookedday id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_bookedday ALTER COLUMN id SET DEFAULT nextval('public.reservations_bookedday_id_seq'::regclass);


--
-- Name: reservations_reservation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_reservation ALTER COLUMN id SET DEFAULT nextval('public.reservations_reservation_id_seq'::regclass);


--
-- Name: reviews_review id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_review ALTER COLUMN id SET DEFAULT nextval('public.reviews_review_id_seq'::regclass);


--
-- Name: rooms_amenity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_amenity ALTER COLUMN id SET DEFAULT nextval('public.rooms_amenity_id_seq'::regclass);


--
-- Name: rooms_facility id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_facility ALTER COLUMN id SET DEFAULT nextval('public.rooms_facility_id_seq'::regclass);


--
-- Name: rooms_houserule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_houserule ALTER COLUMN id SET DEFAULT nextval('public.rooms_houserule_id_seq'::regclass);


--
-- Name: rooms_photo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_photo ALTER COLUMN id SET DEFAULT nextval('public.rooms_photo_id_seq'::regclass);


--
-- Name: rooms_room id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room ALTER COLUMN id SET DEFAULT nextval('public.rooms_room_id_seq'::regclass);


--
-- Name: rooms_room_amenities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_amenities ALTER COLUMN id SET DEFAULT nextval('public.rooms_room_amenities_id_seq'::regclass);


--
-- Name: rooms_room_facilities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_facilities ALTER COLUMN id SET DEFAULT nextval('public.rooms_room_facilities_id_seq'::regclass);


--
-- Name: rooms_room_house_rules id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_house_rules ALTER COLUMN id SET DEFAULT nextval('public.rooms_room_house_rules_id_seq'::regclass);


--
-- Name: rooms_roomtype id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_roomtype ALTER COLUMN id SET DEFAULT nextval('public.rooms_roomtype_id_seq'::regclass);


--
-- Name: tiplists_tiplist id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist ALTER COLUMN id SET DEFAULT nextval('public.tiplists_tiplist_id_seq'::regclass);


--
-- Name: tiplists_tiplist_games id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist_games ALTER COLUMN id SET DEFAULT nextval('public.tiplists_tiplist_games_id_seq'::regclass);


--
-- Name: tiplists_tiplisttutorial id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial ALTER COLUMN id SET DEFAULT nextval('public.tiplists_tiplisttutorial_id_seq'::regclass);


--
-- Name: tiplists_tiplisttutorial_tips id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial_tips ALTER COLUMN id SET DEFAULT nextval('public.tiplists_tiplisttutorial_tips_id_seq'::regclass);


--
-- Name: tips_photo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_photo ALTER COLUMN id SET DEFAULT nextval('public.tips_photo_id_seq'::regclass);


--
-- Name: tips_photo2 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_photo2 ALTER COLUMN id SET DEFAULT nextval('public.tips_photo2_id_seq'::regclass);


--
-- Name: tips_tip id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_tip ALTER COLUMN id SET DEFAULT nextval('public.tips_tip_id_seq'::regclass);


--
-- Name: tips_tiptype id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_tiptype ALTER COLUMN id SET DEFAULT nextval('public.tips_tiptype_id_seq'::regclass);


--
-- Name: users_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user ALTER COLUMN id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Name: users_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups ALTER COLUMN id SET DEFAULT nextval('public.users_user_groups_id_seq'::regclass);


--
-- Name: users_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.users_user_user_permissions_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add user	6	add_user
22	Can change user	6	change_user
23	Can delete user	6	delete_user
24	Can view user	6	view_user
25	Can add room	7	add_room
26	Can change room	7	change_room
27	Can delete room	7	delete_room
28	Can view room	7	view_room
29	Can add Room Type	8	add_roomtype
30	Can change Room Type	8	change_roomtype
31	Can delete Room Type	8	delete_roomtype
32	Can view Room Type	8	view_roomtype
33	Can add amenity	9	add_amenity
34	Can change amenity	9	change_amenity
35	Can delete amenity	9	delete_amenity
36	Can view amenity	9	view_amenity
37	Can add facility	10	add_facility
38	Can change facility	10	change_facility
39	Can delete facility	10	delete_facility
40	Can view facility	10	view_facility
41	Can add House Rule	11	add_houserule
42	Can change House Rule	11	change_houserule
43	Can delete House Rule	11	delete_houserule
44	Can view House Rule	11	view_houserule
45	Can add photo	12	add_photo
46	Can change photo	12	change_photo
47	Can delete photo	12	delete_photo
48	Can view photo	12	view_photo
49	Can add review	13	add_review
50	Can change review	13	change_review
51	Can delete review	13	delete_review
52	Can view review	13	view_review
53	Can add reservation	14	add_reservation
54	Can change reservation	14	change_reservation
55	Can delete reservation	14	delete_reservation
56	Can view reservation	14	view_reservation
57	Can add Booked Day	15	add_bookedday
58	Can change Booked Day	15	change_bookedday
59	Can delete Booked Day	15	delete_bookedday
60	Can view Booked Day	15	view_bookedday
61	Can add list	16	add_list
62	Can change list	16	change_list
63	Can delete list	16	delete_list
64	Can view list	16	view_list
65	Can add conversation	17	add_conversation
66	Can change conversation	17	change_conversation
67	Can delete conversation	17	delete_conversation
68	Can view conversation	17	view_conversation
69	Can add message	18	add_message
70	Can change message	18	change_message
71	Can delete message	18	delete_message
72	Can view message	18	view_message
100	Can add API key	34	add_apikey
101	Can change API key	34	change_apikey
102	Can delete API key	34	delete_apikey
103	Can view API key	34	view_apikey
104	Can add Game Type	35	add_gametype
105	Can change Game Type	35	change_gametype
106	Can delete Game Type	35	delete_gametype
107	Can view Game Type	35	view_gametype
108	Can add photo1	36	add_photo1
109	Can change photo1	36	change_photo1
110	Can delete photo1	36	delete_photo1
111	Can view photo1	36	view_photo1
112	Can add game	37	add_game
113	Can change game	37	change_game
114	Can delete game	37	delete_game
115	Can view game	37	view_game
116	Can add photo2	38	add_photo2
117	Can change photo2	38	change_photo2
118	Can delete photo2	38	delete_photo2
119	Can view photo2	38	view_photo2
120	Can add photo	39	add_photo
121	Can change photo	39	change_photo
122	Can delete photo	39	delete_photo
123	Can view photo	39	view_photo
124	Can add Tip Type	40	add_tiptype
125	Can change Tip Type	40	change_tiptype
126	Can delete Tip Type	40	delete_tiptype
127	Can view Tip Type	40	view_tiptype
128	Can add tip	41	add_tip
129	Can change tip	41	change_tip
130	Can delete tip	41	delete_tip
131	Can view tip	41	view_tip
132	Can add photo2	42	add_photo2
133	Can change photo2	42	change_photo2
134	Can delete photo2	42	delete_photo2
135	Can view photo2	42	view_photo2
136	Can add photo	43	add_photo
137	Can change photo	43	change_photo
138	Can delete photo	43	delete_photo
139	Can view photo	43	view_photo
140	Can add tip list	44	add_tiplist
141	Can change tip list	44	change_tiplist
142	Can delete tip list	44	delete_tiplist
143	Can view tip list	44	view_tiplist
144	Can add tip list tutorial	45	add_tiplisttutorial
145	Can change tip list tutorial	45	change_tiplisttutorial
146	Can delete tip list tutorial	45	delete_tiplisttutorial
147	Can view tip list tutorial	45	view_tiplisttutorial
\.


--
-- Data for Name: conversations_conversation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conversations_conversation (id, created, updated) FROM stdin;
\.


--
-- Data for Name: conversations_conversation_participants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conversations_conversation_participants (id, conversation_id, user_id) FROM stdin;
\.


--
-- Data for Name: conversations_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conversations_message (id, created, updated, message, conversation_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-12-16 08:21:44.038091+00	1	Amenities1	1	[{"added": {}}]	9	1
2	2021-12-16 08:21:50.607071+00	1	Free wifi	1	[{"added": {}}]	10	1
3	2021-12-16 08:22:00.470083+00	1	No Smoking	1	[{"added": {}}]	11	1
4	2021-12-16 08:22:08.512326+00	1	Hotel	1	[{"added": {}}]	8	1
5	2021-12-17 02:26:04.454637+00	1	Api test room	2	[{"changed": {"fields": ["instant_book"]}}]	7	1
6	2021-12-18 07:14:33.826194+00	3	Ola	3		7	1
7	2021-12-18 07:23:54.06551+00	4	Ola	3		7	1
8	2021-12-18 07:25:53.568713+00	5	Ola	3		7	1
9	2021-12-18 07:54:53.744669+00	6	Ola	3		7	1
10	2021-12-22 01:58:05.960832+00	2	versta	3		6	1
11	2021-12-22 02:21:08.423684+00	3	versta	3		6	1
12	2021-12-22 02:21:42.877042+00	4	versta	3		6	1
13	2021-12-22 04:50:47.230462+00	2	Ola	2	[{"changed": {"fields": ["price"]}}]	7	1
14	2021-12-28 03:24:21.868715+00	xkSAuvHu.pbkdf2_sha256$150000$UYmhTMlCcky1$LTi2dDgd5xaqh92li/BLZyMTZ77HJzEB4cwmkZzSHpc=	my-remote-service	3		34	1
15	2021-12-28 03:24:21.876929+00	7S1THZ6q.pbkdf2_sha256$150000$gS4kLSBqDGzE$7X2lalt9+a2GZSFlOXRuaeIEGZovVpl+hk+e7BcawXU=	my-remote-service	3		34	1
16	2021-12-28 03:24:21.879917+00	A1a56Vm8.pbkdf2_sha256$150000$BykqHaNY39UH$E7U9kapFxEEhc3iz4GXo2U51/gM38t7tHFlPJzBHNuA=	my-remote-service	3		34	1
17	2021-12-28 03:24:49.046054+00	sv6nDcQN.pbkdf2_sha256$150000$7RMRSKo0jRxZ$EoFhMjlBlLAb1CoU31rh7IThjmFoF/2IMAcWXz6wjXM=	firts_mylist	1	[{"added": {}}]	34	1
18	2021-12-28 03:31:36.990413+00	VLnBmfOD.pbkdf2_sha256$150000$f7cVwGWiOZMW$+sGfOwwn8pAjoqEHTXS1kFXxwGq0rqkHd7gJuaoamWo=	my-remote-service	3		34	1
19	2022-01-10 08:07:39.779902+00	1	Adventure	1	[{"added": {}}]	35	1
20	2022-01-10 08:13:48.590642+00	2	Watch Notion	1	[{"added": {}}]	37	1
21	2022-01-10 08:24:10.567833+00	3	Watch Norhem	1	[{"added": {}}]	37	1
22	2022-01-10 08:28:08.942893+00	4	Watch Norhem	1	[{"added": {}}]	37	1
23	2022-01-10 08:28:39.48529+00	4	Watch Norhem	3		37	1
24	2022-01-10 08:28:39.4886+00	3	Watch Norhem	3		37	1
25	2022-01-10 08:28:39.492024+00	2	Watch Notion	3		37	1
26	2022-01-10 08:29:12.993471+00	5	Legion	1	[{"added": {}}]	37	1
27	2022-01-10 08:32:17.910286+00	6	Assa	1	[{"added": {}}, {"added": {"name": "photo", "object": "File1"}}]	37	1
28	2022-01-10 08:43:39.335046+00	7	Lianm	1	[{"added": {}}]	37	1
29	2022-01-10 09:24:00.284978+00	1	Windows	1	[{"added": {}}]	40	1
30	2022-01-10 09:25:06.625539+00	1	Cara membuat auto	1	[{"added": {}}, {"added": {"name": "photo", "object": "y files"}}]	41	1
31	2022-01-10 14:29:07.752857+00	8	Test	3		37	1
32	2022-01-11 06:51:55.906154+00	12	gdgdffffffffffffffffff	3		37	1
33	2022-01-13 03:26:53.176509+00	1	My Favourites Games Article	3		44	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	users	user
7	rooms	room
8	rooms	roomtype
9	rooms	amenity
10	rooms	facility
11	rooms	houserule
12	rooms	photo
13	reviews	review
14	reservations	reservation
15	reservations	bookedday
16	lists	list
17	conversations	conversation
18	conversations	message
34	rest_framework_api_key	apikey
35	games	gametype
36	games	photo1
37	games	game
38	games	photo2
39	games	photo
40	tips	tiptype
41	tips	tip
42	tips	photo2
43	tips	photo
44	tiplists	tiplist
45	tiplists	tiplisttutorial
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-12-16 03:39:15.978756+00
2	contenttypes	0002_remove_content_type_name	2021-12-16 03:39:15.992829+00
3	auth	0001_initial	2021-12-16 03:39:16.029515+00
4	auth	0002_alter_permission_name_max_length	2021-12-16 03:39:16.073467+00
5	auth	0003_alter_user_email_max_length	2021-12-16 03:39:16.083004+00
6	auth	0004_alter_user_username_opts	2021-12-16 03:39:16.092208+00
7	auth	0005_alter_user_last_login_null	2021-12-16 03:39:16.099613+00
8	auth	0006_require_contenttypes_0002	2021-12-16 03:39:16.106376+00
9	auth	0007_alter_validators_add_error_messages	2021-12-16 03:39:16.116297+00
10	auth	0008_alter_user_username_max_length	2021-12-16 03:39:16.125919+00
11	auth	0009_alter_user_last_name_max_length	2021-12-16 03:39:16.132536+00
12	auth	0010_alter_group_name_max_length	2021-12-16 03:39:16.144015+00
13	auth	0011_update_proxy_permissions	2021-12-16 03:39:16.152576+00
14	users	0001_initial	2021-12-16 03:39:16.196068+00
15	admin	0001_initial	2021-12-16 03:39:16.256623+00
16	admin	0002_logentry_remove_auto_add	2021-12-16 03:39:16.283002+00
17	admin	0003_logentry_add_action_flag_choices	2021-12-16 03:39:16.294736+00
18	conversations	0001_initial	2021-12-16 03:39:16.340907+00
19	conversations	0002_auto_20190924_1800	2021-12-16 03:39:16.434456+00
20	rooms	0001_initial	2021-12-16 03:39:16.466469+00
21	rooms	0002_auto_20190923_1819	2021-12-16 03:39:16.507099+00
22	rooms	0003_auto_20190924_1436	2021-12-16 03:39:16.648025+00
23	rooms	0004_auto_20190924_1457	2021-12-16 03:39:16.8835+00
24	lists	0001_initial	2021-12-16 03:39:16.928249+00
25	lists	0002_auto_20190924_1800	2021-12-16 03:39:17.013351+00
26	lists	0003_auto_20191119_2016	2021-12-16 03:39:17.045891+00
27	lists	0004_auto_20191218_2035	2021-12-16 03:39:17.072921+00
28	reservations	0001_initial	2021-12-16 03:39:17.10365+00
29	reservations	0002_auto_20190924_1800	2021-12-16 03:39:17.163077+00
30	reservations	0003_bookedday	2021-12-16 03:39:17.194642+00
31	reviews	0001_initial	2021-12-16 03:39:17.231287+00
32	reviews	0002_auto_20190924_1800	2021-12-16 03:39:17.2982+00
33	reviews	0003_auto_20191119_1723	2021-12-16 03:39:17.422424+00
34	rooms	0005_auto_20190924_1739	2021-12-16 03:39:17.448767+00
35	rooms	0006_auto_20190924_1800	2021-12-16 03:39:17.614697+00
36	rooms	0007_auto_20190924_1806	2021-12-16 03:39:17.63861+00
37	rooms	0008_auto_20190925_1555	2021-12-16 03:39:17.658747+00
38	rooms	0009_auto_20191029_1509	2021-12-16 03:39:17.676925+00
39	sessions	0001_initial	2021-12-16 03:39:17.690929+00
40	users	0002_auto_20190925_1555	2021-12-16 03:39:17.719771+00
41	users	0003_auto_20191029_1509	2021-12-16 03:39:17.763286+00
42	users	0004_auto_20191029_1523	2021-12-16 03:39:17.796337+00
43	users	0005_user_login_method	2021-12-16 03:39:17.813472+00
44	users	0006_auto_20191119_1723	2021-12-16 03:39:17.831559+00
45	users	0007_auto_20191119_2016	2021-12-16 03:39:17.873071+00
46	users	0008_auto_20191218_2035	2021-12-16 03:39:17.904663+00
67	rooms	0010_auto_20211220_1823	2021-12-20 09:23:40.758147+00
68	users	0009_user_name	2021-12-20 09:23:40.780809+00
69	users	0010_remove_user_name	2021-12-20 09:24:30.864884+00
70	rest_framework_api_key	0001_initial	2021-12-28 01:28:54.353604+00
71	rest_framework_api_key	0002_auto_20190529_2243	2021-12-28 01:28:54.3789+00
72	rest_framework_api_key	0003_auto_20190623_1952	2021-12-28 01:28:54.391038+00
73	rest_framework_api_key	0004_prefix_hashed_key	2021-12-28 01:28:54.441703+00
74	games	0001_initial	2022-01-10 08:07:00.333373+00
75	games	0002_auto_20220110_1712	2022-01-10 08:13:00.842043+00
76	games	0003_auto_20220110_1727	2022-01-10 08:27:52.835697+00
77	games	0004_game_link_tutorial	2022-01-10 08:42:48.784725+00
78	tips	0001_initial	2022-01-10 09:23:14.535082+00
79	tiplists	0001_initial	2022-01-11 06:51:30.629468+00
80	tiplists	0002_tiplisttutorial	2022-01-11 09:26:17.910478+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
h8t6da5v0inw2zo1xaz1gc4z68k12bqs	ZGY5ODM4YTNiN2Y2NTY5ZjE0ZDc5NDRmYzc5MDg4ZTQ3YzJiOTFkMDp7Il9sYW5ndWFnZSI6ImVuIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjliNGM0ZTA1ZjIwOGJiNmQxOGIxMDg1YWMzMmRjMTAyMmFhZGQ0MDEifQ==	2021-12-31 02:25:51.354141+00
1u61mxmpohmw3jecr6mz5i30hnm98ifq	NWFjYTI4ZjUyYTRhOGFjODJiYmQ3OTg0YWQzMjY1MmJkMDM1YWZhNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjRjNGUwNWYyMDhiYjZkMThiMTA4NWFjMzJkYzEwMjJhYWRkNDAxIn0=	2022-01-04 03:45:11.358851+00
4701vmyaipujfzrly1y54pewrl4tfils	NWFjYTI4ZjUyYTRhOGFjODJiYmQ3OTg0YWQzMjY1MmJkMDM1YWZhNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjRjNGUwNWYyMDhiYjZkMThiMTA4NWFjMzJkYzEwMjJhYWRkNDAxIn0=	2022-01-04 03:52:34.445942+00
9qaqaxtmpt84zstpzl20ldn1stntr7ct	NWFjYTI4ZjUyYTRhOGFjODJiYmQ3OTg0YWQzMjY1MmJkMDM1YWZhNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjRjNGUwNWYyMDhiYjZkMThiMTA4NWFjMzJkYzEwMjJhYWRkNDAxIn0=	2022-01-25 09:53:06.740789+00
y4r5s5310427ecxf9nx1e0t5fp6grex3	YzYyYTFjZmViMGNmYTAyMDYzMjE4ZDlhNmZlMWFlOWEwZWU1MmJiYjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjRjNGUwNWYyMDhiYjZkMThiMTA4NWFjMzJkYzEwMjJhYWRkNDAxIiwiaXNfaG9zdGluZyI6dHJ1ZX0=	2022-01-26 04:43:52.129696+00
\.


--
-- Data for Name: games_game; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.games_game (id, created, updated, name, caption_title, description, game_type_id, host_id, link_tutorial) FROM stdin;
5	2022-01-10 08:29:12.992487+00	2022-01-10 08:29:12.992504+00	Legion	Free GOG	Lorep	1	1	\N
6	2022-01-10 08:32:17.905616+00	2022-01-10 08:32:17.905658+00	Assa	Free on ubisoft	Lorem karoe	1	1	\N
7	2022-01-10 08:43:39.333472+00	2022-01-10 08:43:39.333487+00	Lianm	Test	Good get	1	1	https://www.youtube.com/watch?v=dUoU9I4zD3U&list=OLAK5uy_mpQhSrV2uGWGSvyGFfc44Q0nXnvZHlxcs
9	2022-01-10 14:29:23.320338+00	2022-01-10 14:29:23.320397+00	Test	Cpation	Nice	1	1	link
10	2022-01-11 02:12:46.20114+00	2022-01-11 02:12:46.201158+00	asdadl	alkjakd	asl	1	1	lasdjalkdf
11	2022-01-11 02:20:26.336724+00	2022-01-11 02:20:26.33674+00	asda	jhkak	asdda	1	1	aslsfsff
13	2022-01-12 04:44:22.60916+00	2022-01-12 04:44:22.609174+00	Game1	title this	Lokaie	1	1	linkg
\.


--
-- Data for Name: games_gametype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.games_gametype (id, created, updated, name) FROM stdin;
1	2022-01-10 08:07:39.778529+00	2022-01-10 08:07:39.778545+00	Adventure
\.


--
-- Data for Name: games_photo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.games_photo (id, created, updated, caption, file, game_id) FROM stdin;
1	2022-01-10 08:32:17.90759+00	2022-01-10 08:32:17.907608+00	File1	game_photos/XYplorer_doH79Pn1d1.png	6
\.


--
-- Data for Name: games_photo2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.games_photo2 (id, created, updated, caption, file, game_id) FROM stdin;
\.


--
-- Data for Name: lists_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lists_list (id, created, updated, name, user_id) FROM stdin;
1	2021-12-20 07:57:39.289071+00	2021-12-20 07:57:39.289087+00	My Favourites Houses	1
\.


--
-- Data for Name: lists_list_rooms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lists_list_rooms (id, list_id, room_id) FROM stdin;
6	1	1
\.


--
-- Data for Name: reservations_bookedday; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservations_bookedday (id, created, updated, day, reservation_id) FROM stdin;
\.


--
-- Data for Name: reservations_reservation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservations_reservation (id, created, updated, status, check_in, check_out, guest_id, room_id) FROM stdin;
\.


--
-- Data for Name: rest_framework_api_key_apikey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rest_framework_api_key_apikey (id, created, name, revoked, expiry_date, hashed_key, prefix) FROM stdin;
sv6nDcQN.pbkdf2_sha256$150000$7RMRSKo0jRxZ$EoFhMjlBlLAb1CoU31rh7IThjmFoF/2IMAcWXz6wjXM=	2021-12-28 03:24:49.044685+00	firts_mylist	f	\N	pbkdf2_sha256$150000$7RMRSKo0jRxZ$EoFhMjlBlLAb1CoU31rh7IThjmFoF/2IMAcWXz6wjXM=	sv6nDcQN
\.


--
-- Data for Name: reviews_review; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews_review (id, created, updated, review, accuracy, communication, cleanliness, location, check_in, value, room_id, user_id) FROM stdin;
\.


--
-- Data for Name: rooms_amenity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_amenity (id, created, updated, name) FROM stdin;
1	2021-12-16 08:21:44.036864+00	2021-12-16 08:21:44.036879+00	Amenities1
\.


--
-- Data for Name: rooms_facility; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_facility (id, created, updated, name) FROM stdin;
1	2021-12-16 08:21:50.605966+00	2021-12-16 08:21:50.605982+00	Free wifi
\.


--
-- Data for Name: rooms_houserule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_houserule (id, created, updated, name) FROM stdin;
1	2021-12-16 08:22:00.468997+00	2021-12-16 08:22:00.469014+00	No Smoking
\.


--
-- Data for Name: rooms_photo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_photo (id, created, updated, caption, file, room_id) FROM stdin;
1	2021-12-17 01:33:51.573014+00	2021-12-17 01:33:51.573043+00	test	room_photos/7134013.jpg	1
2	2022-01-06 07:26:19.995991+00	2022-01-06 07:26:19.996006+00	againttest	room_photos/chrome_Iq9u9s30Ii.png	1
\.


--
-- Data for Name: rooms_room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_room (id, created, updated, name, description, country, city, price, address, guests, beds, bedrooms, baths, check_in, check_out, instant_book, host_id, room_type_id) FROM stdin;
8	2021-12-23 01:23:02.925972+00	2021-12-23 01:23:28.390135+00	Liko	Many thing root	ID	Jakarta	100	Street Go	2	2	2	2	00:00:00	12:00:00	t	1	1
2	2021-12-16 09:17:26.120596+00	2021-12-23 02:42:17.371091+00	Ola	We like to send useful feedback to the developers to help them fix bugs and improve the user experience of their open-source projects. Your feedback can help us spot the extension's bugs or any performance issue with them. Please take a few minutes to fill this feedback form. Your valuable input wil	AO	Pokeln	126	Steet 22	1	1	1	1	02:00:00	12:00:00	t	1	1
1	2021-12-16 08:23:19.555178+00	2021-12-23 02:42:49.848958+00	Api test room	Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary	AX	Poken	120	Street	2	2	2	2	01:11:00	12:00:00	t	1	1
9	2022-01-11 02:06:29.233609+00	2022-01-11 02:06:29.233625+00	Contoh	Description	AX	Poki	120	Setliu	1	1	1	1	00:00:00	12:00:00	f	1	1
\.


--
-- Data for Name: rooms_room_amenities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_room_amenities (id, room_id, amenity_id) FROM stdin;
1	8	1
2	2	1
3	1	1
4	9	1
\.


--
-- Data for Name: rooms_room_facilities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_room_facilities (id, room_id, facility_id) FROM stdin;
1	8	1
2	2	1
3	1	1
4	9	1
\.


--
-- Data for Name: rooms_room_house_rules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_room_house_rules (id, room_id, houserule_id) FROM stdin;
1	8	1
2	2	1
3	1	1
4	9	1
\.


--
-- Data for Name: rooms_roomtype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms_roomtype (id, created, updated, name) FROM stdin;
1	2021-12-16 08:22:08.511276+00	2021-12-16 08:22:08.511292+00	Hotel
\.


--
-- Data for Name: tiplists_tiplist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tiplists_tiplist (id, created, updated, name, user_id) FROM stdin;
2	2022-01-13 03:48:34.878387+00	2022-01-13 03:48:34.878407+00	My Favourites Games Article	1
\.


--
-- Data for Name: tiplists_tiplist_games; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tiplists_tiplist_games (id, tiplist_id, game_id) FROM stdin;
\.


--
-- Data for Name: tiplists_tiplisttutorial; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tiplists_tiplisttutorial (id, created, updated, name, user_id) FROM stdin;
1	2022-01-13 03:54:10.422305+00	2022-01-13 03:54:10.422327+00	My Favourites Tips Article	1
\.


--
-- Data for Name: tiplists_tiplisttutorial_tips; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tiplists_tiplisttutorial_tips (id, tiplisttutorial_id, tip_id) FROM stdin;
2	1	1
\.


--
-- Data for Name: tips_photo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tips_photo (id, created, updated, caption, file, "Tip_id") FROM stdin;
1	2022-01-10 09:25:06.623571+00	2022-01-10 09:25:06.623584+00	y files	tip_photos/XYplorer_doH79Pn1d1.png	1
\.


--
-- Data for Name: tips_photo2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tips_photo2 (id, created, updated, caption, file, tip_id) FROM stdin;
\.


--
-- Data for Name: tips_tip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tips_tip (id, created, updated, name, caption_title, description, link_tutorial, host_id, tip_type_id) FROM stdin;
1	2022-01-10 09:25:06.621629+00	2022-01-10 09:25:06.621678+00	Cara membuat auto	Free	Dengan menggunakan sistem yang lebih baiik	https://www.youtube.com/watch?v=dUoU9I4zD3U&list=OLAK5uy_mpQhSrV2uGWGSvyGFfc44Q0nXnvZHlxcs	1	1
2	2022-01-11 09:29:32.972046+00	2022-01-11 09:29:32.972083+00	Cara membuat autokey	Best	Dengan adanya hotkeydapat memudahkan	wwwkasdd	1	1
3	2022-01-13 03:38:58.526666+00	2022-01-13 03:38:58.526683+00	Tip2	jgjhgj	jhghjgj	tertetrtr	1	1
\.


--
-- Data for Name: tips_tiptype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tips_tiptype (id, created, updated, name) FROM stdin;
1	2022-01-10 09:24:00.283272+00	2022-01-10 09:24:00.283298+00	Windows
\.


--
-- Data for Name: users_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, avatar, gender, bio, birthdate, language, currency, superhost, email_verified, email_secret, login_method) FROM stdin;
5	pbkdf2_sha256$150000$4FPzlFhXWtzy$/cbHAdtzzkql81h5gN7LmODwZeOGRCkjRtL0TUoW7+0=	\N	f	versta	max	verstapen	max@max.com	f	t	2021-12-22 03:22:41.490171+00				\N	kr	krw	f	f		email
6	pbkdf2_sha256$150000$Z6M1trlWQzRt$rKBbM87OlQQREOAWrMW7satusbpQSQc3oCc5QAT1jvM=	\N	f	max@max2.com	Unnamed User		max@max2.com	f	t	2021-12-24 08:48:40.694439+00				\N	kr	krw	f	f		email
1	pbkdf2_sha256$150000$DvLrq9XBRCGk$deBPTBOQQ8pPGOCtOXo89dSeA/wh1wkGr+V2ybZcPC4=	2022-01-12 03:19:01.098845+00	t	adminajfpay	Admin		ajfpay@gmail.com	t	t	2021-12-16 08:19:23.621459+00				\N	kr	krw	f	f		email
\.


--
-- Data for Name: users_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: users_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 147, true);


--
-- Name: conversations_conversation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.conversations_conversation_id_seq', 1, false);


--
-- Name: conversations_conversation_participants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.conversations_conversation_participants_id_seq', 1, false);


--
-- Name: conversations_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.conversations_message_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 33, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 45, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 80, true);


--
-- Name: games_game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_game_id_seq', 13, true);


--
-- Name: games_gametype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_gametype_id_seq', 1, true);


--
-- Name: games_photo2_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_photo2_id_seq', 1, false);


--
-- Name: games_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_photo_id_seq', 1, true);


--
-- Name: lists_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lists_list_id_seq', 1, true);


--
-- Name: lists_list_rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lists_list_rooms_id_seq', 6, true);


--
-- Name: reservations_bookedday_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservations_bookedday_id_seq', 1, false);


--
-- Name: reservations_reservation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservations_reservation_id_seq', 1, false);


--
-- Name: reviews_review_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_review_id_seq', 1, false);


--
-- Name: rooms_amenity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_amenity_id_seq', 1, true);


--
-- Name: rooms_facility_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_facility_id_seq', 1, true);


--
-- Name: rooms_houserule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_houserule_id_seq', 1, true);


--
-- Name: rooms_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_photo_id_seq', 2, true);


--
-- Name: rooms_room_amenities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_room_amenities_id_seq', 4, true);


--
-- Name: rooms_room_facilities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_room_facilities_id_seq', 4, true);


--
-- Name: rooms_room_house_rules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_room_house_rules_id_seq', 4, true);


--
-- Name: rooms_room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_room_id_seq', 9, true);


--
-- Name: rooms_roomtype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_roomtype_id_seq', 1, true);


--
-- Name: tiplists_tiplist_games_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tiplists_tiplist_games_id_seq', 5, true);


--
-- Name: tiplists_tiplist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tiplists_tiplist_id_seq', 2, true);


--
-- Name: tiplists_tiplisttutorial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tiplists_tiplisttutorial_id_seq', 1, true);


--
-- Name: tiplists_tiplisttutorial_tips_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tiplists_tiplisttutorial_tips_id_seq', 2, true);


--
-- Name: tips_photo2_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tips_photo2_id_seq', 1, false);


--
-- Name: tips_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tips_photo_id_seq', 1, true);


--
-- Name: tips_tip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tips_tip_id_seq', 3, true);


--
-- Name: tips_tiptype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tips_tiptype_id_seq', 1, true);


--
-- Name: users_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_groups_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_id_seq', 6, true);


--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_user_permissions_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: conversations_conversation_participants conversations_conversati_conversation_id_user_id_9f1ca2f1_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation_participants
    ADD CONSTRAINT conversations_conversati_conversation_id_user_id_9f1ca2f1_uniq UNIQUE (conversation_id, user_id);


--
-- Name: conversations_conversation_participants conversations_conversation_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation_participants
    ADD CONSTRAINT conversations_conversation_participants_pkey PRIMARY KEY (id);


--
-- Name: conversations_conversation conversations_conversation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation
    ADD CONSTRAINT conversations_conversation_pkey PRIMARY KEY (id);


--
-- Name: conversations_message conversations_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_message
    ADD CONSTRAINT conversations_message_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: games_game games_game_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_game
    ADD CONSTRAINT games_game_pkey PRIMARY KEY (id);


--
-- Name: games_gametype games_gametype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_gametype
    ADD CONSTRAINT games_gametype_pkey PRIMARY KEY (id);


--
-- Name: games_photo2 games_photo2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_photo2
    ADD CONSTRAINT games_photo2_pkey PRIMARY KEY (id);


--
-- Name: games_photo games_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_photo
    ADD CONSTRAINT games_photo_pkey PRIMARY KEY (id);


--
-- Name: lists_list lists_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list
    ADD CONSTRAINT lists_list_pkey PRIMARY KEY (id);


--
-- Name: lists_list_rooms lists_list_rooms_list_id_room_id_dc670748_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list_rooms
    ADD CONSTRAINT lists_list_rooms_list_id_room_id_dc670748_uniq UNIQUE (list_id, room_id);


--
-- Name: lists_list_rooms lists_list_rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list_rooms
    ADD CONSTRAINT lists_list_rooms_pkey PRIMARY KEY (id);


--
-- Name: lists_list lists_list_user_id_2731f1b1_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list
    ADD CONSTRAINT lists_list_user_id_2731f1b1_uniq UNIQUE (user_id);


--
-- Name: reservations_bookedday reservations_bookedday_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_bookedday
    ADD CONSTRAINT reservations_bookedday_pkey PRIMARY KEY (id);


--
-- Name: reservations_reservation reservations_reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_reservation
    ADD CONSTRAINT reservations_reservation_pkey PRIMARY KEY (id);


--
-- Name: rest_framework_api_key_apikey rest_framework_api_key_apikey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rest_framework_api_key_apikey
    ADD CONSTRAINT rest_framework_api_key_apikey_pkey PRIMARY KEY (id);


--
-- Name: rest_framework_api_key_apikey rest_framework_api_key_apikey_prefix_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rest_framework_api_key_apikey
    ADD CONSTRAINT rest_framework_api_key_apikey_prefix_key UNIQUE (prefix);


--
-- Name: reviews_review reviews_review_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_review
    ADD CONSTRAINT reviews_review_pkey PRIMARY KEY (id);


--
-- Name: rooms_amenity rooms_amenity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_amenity
    ADD CONSTRAINT rooms_amenity_pkey PRIMARY KEY (id);


--
-- Name: rooms_facility rooms_facility_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_facility
    ADD CONSTRAINT rooms_facility_pkey PRIMARY KEY (id);


--
-- Name: rooms_houserule rooms_houserule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_houserule
    ADD CONSTRAINT rooms_houserule_pkey PRIMARY KEY (id);


--
-- Name: rooms_photo rooms_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_photo
    ADD CONSTRAINT rooms_photo_pkey PRIMARY KEY (id);


--
-- Name: rooms_room_amenities rooms_room_amenities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_amenities
    ADD CONSTRAINT rooms_room_amenities_pkey PRIMARY KEY (id);


--
-- Name: rooms_room_amenities rooms_room_amenities_room_id_amenity_id_de123f77_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_amenities
    ADD CONSTRAINT rooms_room_amenities_room_id_amenity_id_de123f77_uniq UNIQUE (room_id, amenity_id);


--
-- Name: rooms_room_facilities rooms_room_facilities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_facilities
    ADD CONSTRAINT rooms_room_facilities_pkey PRIMARY KEY (id);


--
-- Name: rooms_room_facilities rooms_room_facilities_room_id_facility_id_515551fb_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_facilities
    ADD CONSTRAINT rooms_room_facilities_room_id_facility_id_515551fb_uniq UNIQUE (room_id, facility_id);


--
-- Name: rooms_room_house_rules rooms_room_house_rules_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_house_rules
    ADD CONSTRAINT rooms_room_house_rules_pkey PRIMARY KEY (id);


--
-- Name: rooms_room_house_rules rooms_room_house_rules_room_id_houserule_id_28060e19_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_house_rules
    ADD CONSTRAINT rooms_room_house_rules_room_id_houserule_id_28060e19_uniq UNIQUE (room_id, houserule_id);


--
-- Name: rooms_room rooms_room_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room
    ADD CONSTRAINT rooms_room_pkey PRIMARY KEY (id);


--
-- Name: rooms_roomtype rooms_roomtype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_roomtype
    ADD CONSTRAINT rooms_roomtype_pkey PRIMARY KEY (id);


--
-- Name: tiplists_tiplist_games tiplists_tiplist_games_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist_games
    ADD CONSTRAINT tiplists_tiplist_games_pkey PRIMARY KEY (id);


--
-- Name: tiplists_tiplist_games tiplists_tiplist_games_tiplist_id_game_id_0e981b9f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist_games
    ADD CONSTRAINT tiplists_tiplist_games_tiplist_id_game_id_0e981b9f_uniq UNIQUE (tiplist_id, game_id);


--
-- Name: tiplists_tiplist tiplists_tiplist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist
    ADD CONSTRAINT tiplists_tiplist_pkey PRIMARY KEY (id);


--
-- Name: tiplists_tiplist tiplists_tiplist_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist
    ADD CONSTRAINT tiplists_tiplist_user_id_key UNIQUE (user_id);


--
-- Name: tiplists_tiplisttutorial tiplists_tiplisttutorial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial
    ADD CONSTRAINT tiplists_tiplisttutorial_pkey PRIMARY KEY (id);


--
-- Name: tiplists_tiplisttutorial_tips tiplists_tiplisttutorial_tiplisttutorial_id_tip_i_ac434802_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial_tips
    ADD CONSTRAINT tiplists_tiplisttutorial_tiplisttutorial_id_tip_i_ac434802_uniq UNIQUE (tiplisttutorial_id, tip_id);


--
-- Name: tiplists_tiplisttutorial_tips tiplists_tiplisttutorial_tips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial_tips
    ADD CONSTRAINT tiplists_tiplisttutorial_tips_pkey PRIMARY KEY (id);


--
-- Name: tiplists_tiplisttutorial tiplists_tiplisttutorial_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial
    ADD CONSTRAINT tiplists_tiplisttutorial_user_id_key UNIQUE (user_id);


--
-- Name: tips_photo2 tips_photo2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_photo2
    ADD CONSTRAINT tips_photo2_pkey PRIMARY KEY (id);


--
-- Name: tips_photo tips_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_photo
    ADD CONSTRAINT tips_photo_pkey PRIMARY KEY (id);


--
-- Name: tips_tip tips_tip_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_tip
    ADD CONSTRAINT tips_tip_pkey PRIMARY KEY (id);


--
-- Name: tips_tiptype tips_tiptype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_tiptype
    ADD CONSTRAINT tips_tiptype_pkey PRIMARY KEY (id);


--
-- Name: users_user_groups users_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_pkey PRIMARY KEY (id);


--
-- Name: users_user_groups users_user_groups_user_id_group_id_b88eab82_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_user_id_group_id_b88eab82_uniq UNIQUE (user_id, group_id);


--
-- Name: users_user users_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_pkey PRIMARY KEY (id);


--
-- Name: users_user_user_permissions users_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: users_user_user_permissions users_user_user_permissions_user_id_permission_id_43338c45_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_user_id_permission_id_43338c45_uniq UNIQUE (user_id, permission_id);


--
-- Name: users_user users_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_username_key UNIQUE (username);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: conversations_conversation_conversation_id_35dc4d12; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX conversations_conversation_conversation_id_35dc4d12 ON public.conversations_conversation_participants USING btree (conversation_id);


--
-- Name: conversations_conversation_participants_user_id_ce81e395; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX conversations_conversation_participants_user_id_ce81e395 ON public.conversations_conversation_participants USING btree (user_id);


--
-- Name: conversations_message_conversation_id_f1a2d5e9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX conversations_message_conversation_id_f1a2d5e9 ON public.conversations_message USING btree (conversation_id);


--
-- Name: conversations_message_user_id_a91d73be; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX conversations_message_user_id_a91d73be ON public.conversations_message USING btree (user_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: games_game_game_type_id_2fefd73e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX games_game_game_type_id_2fefd73e ON public.games_game USING btree (game_type_id);


--
-- Name: games_game_host_id_651375fe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX games_game_host_id_651375fe ON public.games_game USING btree (host_id);


--
-- Name: games_photo2_game_id_12f7b859; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX games_photo2_game_id_12f7b859 ON public.games_photo2 USING btree (game_id);


--
-- Name: games_photo_game_id_4af7db4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX games_photo_game_id_4af7db4b ON public.games_photo USING btree (game_id);


--
-- Name: lists_list_rooms_list_id_a4eea0a4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lists_list_rooms_list_id_a4eea0a4 ON public.lists_list_rooms USING btree (list_id);


--
-- Name: lists_list_rooms_room_id_94f17871; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lists_list_rooms_room_id_94f17871 ON public.lists_list_rooms USING btree (room_id);


--
-- Name: reservations_bookedday_reservation_id_d65f8ff7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reservations_bookedday_reservation_id_d65f8ff7 ON public.reservations_bookedday USING btree (reservation_id);


--
-- Name: reservations_reservation_guest_id_c59ab314; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reservations_reservation_guest_id_c59ab314 ON public.reservations_reservation USING btree (guest_id);


--
-- Name: reservations_reservation_room_id_f7d9ba76; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reservations_reservation_room_id_f7d9ba76 ON public.reservations_reservation USING btree (room_id);


--
-- Name: rest_framework_api_key_apikey_created_c61872d9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rest_framework_api_key_apikey_created_c61872d9 ON public.rest_framework_api_key_apikey USING btree (created);


--
-- Name: rest_framework_api_key_apikey_id_6e07e68e_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rest_framework_api_key_apikey_id_6e07e68e_like ON public.rest_framework_api_key_apikey USING btree (id varchar_pattern_ops);


--
-- Name: rest_framework_api_key_apikey_prefix_4e0db5f8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rest_framework_api_key_apikey_prefix_4e0db5f8_like ON public.rest_framework_api_key_apikey USING btree (prefix varchar_pattern_ops);


--
-- Name: reviews_review_room_id_88f19e2a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reviews_review_room_id_88f19e2a ON public.reviews_review USING btree (room_id);


--
-- Name: reviews_review_user_id_875caff2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reviews_review_user_id_875caff2 ON public.reviews_review USING btree (user_id);


--
-- Name: rooms_photo_room_id_fce0d443; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_photo_room_id_fce0d443 ON public.rooms_photo USING btree (room_id);


--
-- Name: rooms_room_amenities_amenity_id_70bec57a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_amenities_amenity_id_70bec57a ON public.rooms_room_amenities USING btree (amenity_id);


--
-- Name: rooms_room_amenities_room_id_62a7a61d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_amenities_room_id_62a7a61d ON public.rooms_room_amenities USING btree (room_id);


--
-- Name: rooms_room_facilities_facility_id_ad19e8df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_facilities_facility_id_ad19e8df ON public.rooms_room_facilities USING btree (facility_id);


--
-- Name: rooms_room_facilities_room_id_ef83b10e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_facilities_room_id_ef83b10e ON public.rooms_room_facilities USING btree (room_id);


--
-- Name: rooms_room_host_id_7f45f18c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_host_id_7f45f18c ON public.rooms_room USING btree (host_id);


--
-- Name: rooms_room_house_rules_houserule_id_06fd6f92; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_house_rules_houserule_id_06fd6f92 ON public.rooms_room_house_rules USING btree (houserule_id);


--
-- Name: rooms_room_house_rules_room_id_bfccab7a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_house_rules_room_id_bfccab7a ON public.rooms_room_house_rules USING btree (room_id);


--
-- Name: rooms_room_room_type_id_d6bd9615; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rooms_room_room_type_id_d6bd9615 ON public.rooms_room USING btree (room_type_id);


--
-- Name: tiplists_tiplist_games_game_id_17b6a93b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tiplists_tiplist_games_game_id_17b6a93b ON public.tiplists_tiplist_games USING btree (game_id);


--
-- Name: tiplists_tiplist_games_tiplist_id_e226dd86; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tiplists_tiplist_games_tiplist_id_e226dd86 ON public.tiplists_tiplist_games USING btree (tiplist_id);


--
-- Name: tiplists_tiplisttutorial_tips_tip_id_6d506311; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tiplists_tiplisttutorial_tips_tip_id_6d506311 ON public.tiplists_tiplisttutorial_tips USING btree (tip_id);


--
-- Name: tiplists_tiplisttutorial_tips_tiplisttutorial_id_0718364f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tiplists_tiplisttutorial_tips_tiplisttutorial_id_0718364f ON public.tiplists_tiplisttutorial_tips USING btree (tiplisttutorial_id);


--
-- Name: tips_photo2_tip_id_2fa2a39b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tips_photo2_tip_id_2fa2a39b ON public.tips_photo2 USING btree (tip_id);


--
-- Name: tips_photo_Tip_id_c7536887; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "tips_photo_Tip_id_c7536887" ON public.tips_photo USING btree ("Tip_id");


--
-- Name: tips_tip_host_id_a78aa2e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tips_tip_host_id_a78aa2e6 ON public.tips_tip USING btree (host_id);


--
-- Name: tips_tip_tip_type_id_cc5c65bd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tips_tip_tip_type_id_cc5c65bd ON public.tips_tip USING btree (tip_type_id);


--
-- Name: users_user_groups_group_id_9afc8d0e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_groups_group_id_9afc8d0e ON public.users_user_groups USING btree (group_id);


--
-- Name: users_user_groups_user_id_5f6f5a90; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_groups_user_id_5f6f5a90 ON public.users_user_groups USING btree (user_id);


--
-- Name: users_user_user_permissions_permission_id_0b93982e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_user_permissions_permission_id_0b93982e ON public.users_user_user_permissions USING btree (permission_id);


--
-- Name: users_user_user_permissions_user_id_20aca447; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_user_permissions_user_id_20aca447 ON public.users_user_user_permissions USING btree (user_id);


--
-- Name: users_user_username_06e46fe6_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_username_06e46fe6_like ON public.users_user USING btree (username varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: conversations_conversation_participants conversations_conver_conversation_id_35dc4d12_fk_conversat; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation_participants
    ADD CONSTRAINT conversations_conver_conversation_id_35dc4d12_fk_conversat FOREIGN KEY (conversation_id) REFERENCES public.conversations_conversation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: conversations_conversation_participants conversations_conver_user_id_ce81e395_fk_users_use; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_conversation_participants
    ADD CONSTRAINT conversations_conver_user_id_ce81e395_fk_users_use FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: conversations_message conversations_messag_conversation_id_f1a2d5e9_fk_conversat; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_message
    ADD CONSTRAINT conversations_messag_conversation_id_f1a2d5e9_fk_conversat FOREIGN KEY (conversation_id) REFERENCES public.conversations_conversation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: conversations_message conversations_message_user_id_a91d73be_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversations_message
    ADD CONSTRAINT conversations_message_user_id_a91d73be_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: games_game games_game_game_type_id_2fefd73e_fk_games_gametype_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_game
    ADD CONSTRAINT games_game_game_type_id_2fefd73e_fk_games_gametype_id FOREIGN KEY (game_type_id) REFERENCES public.games_gametype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: games_game games_game_host_id_651375fe_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_game
    ADD CONSTRAINT games_game_host_id_651375fe_fk_users_user_id FOREIGN KEY (host_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: games_photo2 games_photo2_game_id_12f7b859_fk_games_game_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_photo2
    ADD CONSTRAINT games_photo2_game_id_12f7b859_fk_games_game_id FOREIGN KEY (game_id) REFERENCES public.games_game(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: games_photo games_photo_game_id_4af7db4b_fk_games_game_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games_photo
    ADD CONSTRAINT games_photo_game_id_4af7db4b_fk_games_game_id FOREIGN KEY (game_id) REFERENCES public.games_game(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lists_list_rooms lists_list_rooms_list_id_a4eea0a4_fk_lists_list_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list_rooms
    ADD CONSTRAINT lists_list_rooms_list_id_a4eea0a4_fk_lists_list_id FOREIGN KEY (list_id) REFERENCES public.lists_list(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lists_list_rooms lists_list_rooms_room_id_94f17871_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list_rooms
    ADD CONSTRAINT lists_list_rooms_room_id_94f17871_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lists_list lists_list_user_id_2731f1b1_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists_list
    ADD CONSTRAINT lists_list_user_id_2731f1b1_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: reservations_bookedday reservations_bookedd_reservation_id_d65f8ff7_fk_reservati; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_bookedday
    ADD CONSTRAINT reservations_bookedd_reservation_id_d65f8ff7_fk_reservati FOREIGN KEY (reservation_id) REFERENCES public.reservations_reservation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: reservations_reservation reservations_reservation_guest_id_c59ab314_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_reservation
    ADD CONSTRAINT reservations_reservation_guest_id_c59ab314_fk_users_user_id FOREIGN KEY (guest_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: reservations_reservation reservations_reservation_room_id_f7d9ba76_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations_reservation
    ADD CONSTRAINT reservations_reservation_room_id_f7d9ba76_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: reviews_review reviews_review_room_id_88f19e2a_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_review
    ADD CONSTRAINT reviews_review_room_id_88f19e2a_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: reviews_review reviews_review_user_id_875caff2_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_review
    ADD CONSTRAINT reviews_review_user_id_875caff2_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_photo rooms_photo_room_id_fce0d443_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_photo
    ADD CONSTRAINT rooms_photo_room_id_fce0d443_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room_amenities rooms_room_amenities_amenity_id_70bec57a_fk_rooms_amenity_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_amenities
    ADD CONSTRAINT rooms_room_amenities_amenity_id_70bec57a_fk_rooms_amenity_id FOREIGN KEY (amenity_id) REFERENCES public.rooms_amenity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room_amenities rooms_room_amenities_room_id_62a7a61d_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_amenities
    ADD CONSTRAINT rooms_room_amenities_room_id_62a7a61d_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room_facilities rooms_room_facilities_facility_id_ad19e8df_fk_rooms_facility_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_facilities
    ADD CONSTRAINT rooms_room_facilities_facility_id_ad19e8df_fk_rooms_facility_id FOREIGN KEY (facility_id) REFERENCES public.rooms_facility(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room_facilities rooms_room_facilities_room_id_ef83b10e_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_facilities
    ADD CONSTRAINT rooms_room_facilities_room_id_ef83b10e_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room rooms_room_host_id_7f45f18c_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room
    ADD CONSTRAINT rooms_room_host_id_7f45f18c_fk_users_user_id FOREIGN KEY (host_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room_house_rules rooms_room_house_rul_houserule_id_06fd6f92_fk_rooms_hou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_house_rules
    ADD CONSTRAINT rooms_room_house_rul_houserule_id_06fd6f92_fk_rooms_hou FOREIGN KEY (houserule_id) REFERENCES public.rooms_houserule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room_house_rules rooms_room_house_rules_room_id_bfccab7a_fk_rooms_room_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room_house_rules
    ADD CONSTRAINT rooms_room_house_rules_room_id_bfccab7a_fk_rooms_room_id FOREIGN KEY (room_id) REFERENCES public.rooms_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rooms_room rooms_room_room_type_id_d6bd9615_fk_rooms_roomtype_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms_room
    ADD CONSTRAINT rooms_room_room_type_id_d6bd9615_fk_rooms_roomtype_id FOREIGN KEY (room_type_id) REFERENCES public.rooms_roomtype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tiplists_tiplist_games tiplists_tiplist_gam_tiplist_id_e226dd86_fk_tiplists_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist_games
    ADD CONSTRAINT tiplists_tiplist_gam_tiplist_id_e226dd86_fk_tiplists_ FOREIGN KEY (tiplist_id) REFERENCES public.tiplists_tiplist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tiplists_tiplist_games tiplists_tiplist_games_game_id_17b6a93b_fk_games_game_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist_games
    ADD CONSTRAINT tiplists_tiplist_games_game_id_17b6a93b_fk_games_game_id FOREIGN KEY (game_id) REFERENCES public.games_game(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tiplists_tiplist tiplists_tiplist_user_id_621cc61d_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplist
    ADD CONSTRAINT tiplists_tiplist_user_id_621cc61d_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tiplists_tiplisttutorial_tips tiplists_tiplisttuto_tiplisttutorial_id_0718364f_fk_tiplists_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial_tips
    ADD CONSTRAINT tiplists_tiplisttuto_tiplisttutorial_id_0718364f_fk_tiplists_ FOREIGN KEY (tiplisttutorial_id) REFERENCES public.tiplists_tiplisttutorial(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tiplists_tiplisttutorial_tips tiplists_tiplisttutorial_tips_tip_id_6d506311_fk_tips_tip_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial_tips
    ADD CONSTRAINT tiplists_tiplisttutorial_tips_tip_id_6d506311_fk_tips_tip_id FOREIGN KEY (tip_id) REFERENCES public.tips_tip(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tiplists_tiplisttutorial tiplists_tiplisttutorial_user_id_1726411c_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiplists_tiplisttutorial
    ADD CONSTRAINT tiplists_tiplisttutorial_user_id_1726411c_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tips_photo2 tips_photo2_tip_id_2fa2a39b_fk_tips_tip_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_photo2
    ADD CONSTRAINT tips_photo2_tip_id_2fa2a39b_fk_tips_tip_id FOREIGN KEY (tip_id) REFERENCES public.tips_tip(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tips_photo tips_photo_Tip_id_c7536887_fk_tips_tip_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_photo
    ADD CONSTRAINT "tips_photo_Tip_id_c7536887_fk_tips_tip_id" FOREIGN KEY ("Tip_id") REFERENCES public.tips_tip(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tips_tip tips_tip_host_id_a78aa2e6_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_tip
    ADD CONSTRAINT tips_tip_host_id_a78aa2e6_fk_users_user_id FOREIGN KEY (host_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tips_tip tips_tip_tip_type_id_cc5c65bd_fk_tips_tiptype_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tips_tip
    ADD CONSTRAINT tips_tip_tip_type_id_cc5c65bd_fk_tips_tiptype_id FOREIGN KEY (tip_type_id) REFERENCES public.tips_tiptype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_groups users_user_groups_group_id_9afc8d0e_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_group_id_9afc8d0e_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_groups users_user_groups_user_id_5f6f5a90_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_user_id_5f6f5a90_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_user_permissions users_user_user_perm_permission_id_0b93982e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_perm_permission_id_0b93982e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_user_permissions users_user_user_permissions_user_id_20aca447_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_user_id_20aca447_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

